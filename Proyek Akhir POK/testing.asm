.include "m16def.inc"

.cseg
.org $30
LED_STATUS:
.db 0,1,2

.def temp = r16
.def temp1 = r17

ldi ZH, 0x00
	ldi ZL, 0x60

	ldi temp, 1
	st Z, temp
	adiw ZL, 1
	
	st Z, temp
	adiw ZL, 1

	st Z, temp
	adiw ZL, 1

	ldi temp, 0
	ldi ZH, 0x00
	ldi ZL, 0x60
	
	ld temp1, Z
	or temp, temp1
	lsl temp
	adiw ZL, 1

	ld temp1, Z
	or temp, temp1
	lsl temp
	adiw ZL, 1

	ld temp1, Z
	or temp, temp1
	adiw ZL, 1
	
	lsl temp
	lsl temp
	lsl temp
	
	;out to LEDs
	out PORTA, temp

forever: rjmp forever
