Program Booking Table
	Program yang digunakan untuk mensimulasikan pemesanan meja di restaurant/rumah makan. Di program ini, pengguna dapat melihat status setiap meja di restauran ini.
	Selain itu, pengguna dapat memesan meja, dan meninggalkan meja ketika sudah selesai dipakai.

	Program menggunakan AVR Atmega16 dengan keypad, dan button(berlabel book dan leave) sebagai input, LED sebagai status meja saat ini(menyala berarti available), dan LCD sebagai alat interaktif pengguna dengan program ini.

	Alur menggunakan:
		- Setelah dijalankan, status awalnya ada semua meja berstatus available(semua lampu akan menyala).
		- Jika ingin memesan table, pertama klik button berlabel book. Lalu di LCD akan menampilkan "Book Table:"
		- Lalu input meja yang ingin dipesan dengan keypad. Keypad yang available hanya keypad 1, 2, 3 dikarenakan meja yang tersedia hanya 3
		- Jika sudah, lalu tekan enter pada keypad. Jika meja belum dipesan, makan LCD akan menampilkan "booked", dan meja telah dipesan. Jika meja telah dipesan, maka LCD akan menampilkan "invalid/failed"
		- Jika ingin meninggalkan table, klik button berlabel leave. Di LCD akan menampilkan "Leave Table:"
		- Lalu input meja yang ingin ditinggalkan dengan keypad. 
		- Jika sudah, lalu tekan enter pada keypad. Setelah itu, di LCD akan menampilkan "Leaved"